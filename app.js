(async () => {
  const express = require('express');
  const app = express();
  const path = require('path');
  const server = require('http').createServer(app);
  const io = require('socket.io')(server);
  const port = process.env.PORT || 3000;
  const nats = require('nats');

  function getRandonString(length) {
    var chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var charLength = chars.length;
    var result = '';
    for (var i = 0; i < length; i++) {
      result += chars.charAt(Math.floor(Math.random() * charLength));
    }
    return result;
  }

  const connection = await nats.connect({ servers: 'nats://nats' });

  const codec = nats.JSONCodec();

  server.listen(port, () => {
    console.log('Server listening at port %d', port);
  });

  io.on('connection', async (socket) => {
    const node_id = socket.handshake.query.id;
    const include_duplicates = false; // not supported atm
    const include_children = socket.handshake.query.include == 'children';

    const buffer = codec.encode(
      {
        _id: node_id,
        recursive: false,
        include_attributes: false,
        calculate_attribute_display: false,
      }
    );

    let response = await connection.request(
      'structure.rpc.nodes_get',
      buffer
    );
    response = codec.decode(response.data)
    response = response.result

    if (response.length != 1) {
      console.error(`returned ${response.response.length} nodes`);
      return;
    }

    const node = response[0];

    let topic = node._id;
    if (node.parentPath != '') {
      topic =
        node.parentPath.substring(1).replaceAll('/', '.') + '.' + node._id;
    }

    topic = 'structure.nodes.updates.' + topic;

    const callback = async (error, message) => {
      const body = codec.decode(message.data);
      console.log(body)

      if (body.subject == 'node.attributes' && !include_duplicates) {
        body.data.node.attributes = body.data.node.attributes.filter(
          (attribute) => attribute.changed
        );
        if (body.data.node.attributes.length == 0) {
          return;
        }
      }

      await socket.emit('node.update', body);
    }

    const subscriptions = [connection.subscribe(topic, {callback})]

    if (include_children) {
      // append wildcard
      subscriptions.push(connection.subscribe(`${topic}.>`, {callback}));
    }

    console.log(`subbed on topic ${topic}`)
    socket.on('disconnect', () => {
      for(const subscription of subscriptions) {
        subscription.unsubscribe()
      }
    });
  });

  console.log('started');
})();
